import java.sql.Array;
import java.util.*;

public class Main {

    private static List<Candidat> candidats = new LinkedList<Candidat>(){{
        add(new Candidat("Никита", 0));
        add(new Candidat("Ирина", 0));
        add(new Candidat("Саша", 0));
    }};

    public static void main(String[] args) throws Exception {

        System.out.println("Выбор старосты группы. метод Борда");
        System.out.println("Введите количество голосующих людей");


        Scanner scanner = new Scanner(System.in);
        Integer electorCount = scanner.nextInt();

        System.out.println("Голосуйте " + electorCount + " раз(а)");




        //голосование n раз
        for (int i = 0; i < electorCount; i++){

            System.out.println("===============================" );
            System.out.println("голосует " + i + "-й избератель" );
            System.out.println("===============================" );

            //вывод на просмотр всех кандидатов
            for (int j = 0; j < candidats.size(); j++) {
                System.out.println(j + " - " + candidats.get(j).getName());
            }
            System.out.println("===============================" );

            //переменная с порядком кандидатов для этого голосующего
            Integer[] places = new Integer[candidats.size()];


            //пока не заполним лист с расположенными местами кандидатов
            while (Arrays.stream(places).anyMatch(Objects::isNull)){

                for (int j = 0; j < candidats.size(); j++) {
                    System.out.println("расположеите кандидата на свободное место.");
                    System.out.println(j + " - " + candidats.get(j).getName());

                    //вывод всех расположенных по местам или null на пустых местах
                    System.out.println("текущее расположение кандидатов:");
                    for (int k = 0; k < candidats.size(); k++) {
                        System.out.println(k + 1 + ") " + (places[k] == null ? "" : candidats.get(places[k]).getName()));
                    }

                    Integer votePlace = scanner.nextInt() - 1;
                    places[votePlace] = j;
                }

            }

            //пройдя по всем places заполняем рейтинг кандидатов
            for (int j = 0; j < candidats.size(); j++) {
                candidats.get(places[j]).setRating(
                        candidats.get(places[j]).getRating() + candidats.size() - j - 1
                ); //очки
            }


        }


        //вывод на просмотр с их рейтингом
        for (int i = 0; i < candidats.size(); i++) {
            System.out.println(
                    i
                            + " - "
                            + candidats.get(i).getName()
                            + ", рейтинг: "
                            + candidats.get(i).getRating()
            );
        }

        System.out.println("===============================" );
        System.out.println("Итоговый рейтинг" );

        //поиск кандидата, который никому не уступал в сравнении (абсолютного победителя)
        Integer maxRating = candidats.stream().mapToInt(Candidat::getRating).max().orElseThrow(() -> new Exception("рейтинг не найден"));

        if (candidats.stream().filter(candidat -> candidat.getRating().equals(maxRating)).count() > 1){
            System.out.println("Нет одназначного победителя. несколько кандидатов набрали рейтинг равный " + maxRating);
        } else {
            Candidat winner = candidats.stream().filter(candidat -> candidat.getRating().equals(maxRating)).findFirst().orElseThrow(() -> new Exception("Нет победителя"));
            System.out.println("Победил кандидат: " + winner.getName() + ", так как его рейтинг по правилу Симпсона больше остальных.");
        }
    }
}
